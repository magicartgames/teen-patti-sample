﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

[RequireComponent(typeof(Image))]
public class CardSlot : MonoBehaviour, IPointerClickHandler {
	
	public Sprite m_hiddenSprite;
	private Image m_cardGraphic;
	[HideInInspector]
	public CardData m_card;
	public bool m_isHidden;
	public UnityAction<CardSlot> e_OnClickCard;
	
	
	void Awake() {
		m_cardGraphic = GetComponent<Image>();
	}
	
	// Use this for initialization
	void Start () {
		if (m_card == null) {
			m_isHidden = true;
			m_cardGraphic.sprite = m_hiddenSprite;
		}
		//SetCartData(MatchManager.GetCardFromDeck());
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void SetCartData(CardData carddata) {
		m_card = carddata;
		m_isHidden = true;
		m_cardGraphic.sprite = m_hiddenSprite;
	}
	
	public void SetHidden(bool hidden) {
		if (m_card != null) {
			m_isHidden = hidden;
			m_cardGraphic.sprite = hidden? m_hiddenSprite : m_card.sprite;
		}
	}
	
	
	// Clicked on a Card
	public void OnPointerClick(PointerEventData pointerEvent) {
		if (e_OnClickCard != null) {
			e_OnClickCard(this);
		}
		//SetCartData(MatchManager.PickCardFromDeck());
	}
}
