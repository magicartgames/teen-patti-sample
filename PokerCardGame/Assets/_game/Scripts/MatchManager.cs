﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using TMPro;
using UnityEngine.Networking;

public class MatchManager : NetworkBehaviour {
	public static MatchManager _instance;
	public enum HANDTYPE {HIGH_CARD, PAIR, FLUSH, STRAIGHT, STRAIGHT_FLUSH, TRAIL}
	
	public TextMeshProUGUI m_potText;
	public GameObject m_winnerPanel;
	
	List<Player> m_players;
	Player m_localPlayer;
	Player m_remotePlayer;
	public Player m_currentTurnPlayer;
	
	public List<CardData> m_allCards;
	private List<int> m_usedCardIds;
	private static List<CardData> m_deck;
	
	[SyncVar]
	public int m_playerTurn = 0;
	
	[SyncVar]
	public int m_pot = 0;
	
	[SyncVar]
	public int m_lastBetTotal = 0;
	
	void Awake() {
		_instance = this;
		for (int i=0; i < m_allCards.Count; i++) {
			m_allCards[i].cardId = i;
		}
		m_deck = new List<CardData>(m_allCards.ToArray());
		m_players = new List<Player>();
	}
	
	public void AddPlayer(Player player) {
		m_players.Add(player);
		//Debug.Log("Added Player: " + player.name);
		
		// All playeres are in the game now 
		if (m_players.Count == 2) {
			StartCoroutine(StartGame());
		}
	}
	
	public void AddLocalPlayer(Player player) {
		m_localPlayer = player;
	}
	
	public void AddRemotePlayer(Player player) {
		m_remotePlayer = player;
	}
	
	// ====================================================== SERVER LOGIC METHODS ==============================>
	/// <summary>
	/// After the two players are initialized, StartGame must be called to initialize the board for another round.
	/// </summary>
	/// <returns></returns>
	[Server]
	public IEnumerator StartGame() {
		m_usedCardIds = new List<int>();
		//Debug.Log("Waiting for connection is ready... Time: " + Time.time);
		yield return new WaitForSeconds(0.5f); //StartCoroutine(WaitForConnectionIsReady());	
		//Debug.Log("Done... Finish Time: " + Time.time);
		m_players.ForEach(p => p.RpcInitializeRound());
		m_players.ForEach(p => p.CmdAnteBet(50));
		ServerNextPlayerTurn();
	}
	
	[Server]
	public IEnumerator WaitForConnectionIsReady() {
		while (connectionToClient == null || !connectionToClient.isReady) {
			yield return null;
		}
	}
	
	
	[Server]
	public void ServerNextPlayerTurn() {
		//m_lastBetTotal = 0;
		int currentBet = m_currentTurnPlayer != null? m_currentTurnPlayer.m_currentBet : 50;
		m_lastBetTotal =  currentBet - m_lastBetTotal;
		//Debug.Log("PLAYER PREVIOUS BET: " + currentBet + " - LAST BET TOTAL: " + m_lastBetTotal + " DIFFERENCE IS: " + (currentBet - m_lastBetTotal));
		RpcUpdateLastBet(m_lastBetTotal);
		m_currentTurnPlayer = m_players[m_playerTurn%m_players.Count];
		m_players.ForEach(p => p.RpcPlayerTurn(m_currentTurnPlayer.m_name, m_lastBetTotal));
		m_playerTurn++;
	}
	
	[ClientRpc]
	public void RpcUpdateLastBet(int value) {
		//Debug.Log("UPDATING LAST PLAYER BET IS IN CLIENT: " + value);
		MatchManager._instance.m_lastBetTotal = value; 
	}
	
	[Server]
	public void DelayedNextTurn() {
		
		
	}
	
	[Server]
	public bool IsMyTurn(Player p) {
		return p == m_players[m_playerTurn%m_players.Count];
	}
	

	public CardData PickCardFromDeck() {
		int cardIndex;
		do {
			cardIndex = Random.Range(0, m_deck.Count);	
		} while(m_usedCardIds.Contains(cardIndex));
		m_usedCardIds.Add(cardIndex);
		
		return m_allCards[cardIndex];
	}
	
	public CardData GetCard(int cardId) {
		return m_allCards.Find(card => card.cardId == cardId);
	}
	
	[Server]
	public void ServerEndTurn(int turnValue) {
		RpcSyncTurn(turnValue);
	}
	
	[Server]
	public void PlayerFold(string m_name) {
		
		var loserPlayer = m_players.Find(p=>p.m_name==m_name);
		var winnerPlayer = m_players.Find(p=>p.m_name!=m_name);
				
		winnerPlayer.m_cash += m_pot;
		RpcShowWinnerPanel(winnerPlayer.m_name, winnerPlayer.m_playerHandIds.ToArray(), loserPlayer.m_name, loserPlayer.m_playerHandIds.ToArray(), m_pot);
		Invoke("StartNewRound", 5f);
	}
	
	[Server]
	void StartNewRound() {
		//Debug.Log("STARTING NEW ROUND.");
		m_playerTurn = 0;
		m_pot = 0;
		m_lastBetTotal = 0;
		RpcHideWinnerPanel();
		StartCoroutine(StartGame());
	}
	
	[Server]
	public void ShowCards() {
		Player p1 = m_players[0];
		Player p2 = m_players[1];
		
		DecideWinner(p1, p2);
	}
	
	
	
	[Server]
	public void IncreasePot(int value) {
		m_pot += value;
		RpcSyncPot(m_pot);
	}
	
	[ClientRpc]
	public void RpcSyncTurn(int currentTurn) {
		m_playerTurn = currentTurn;
		Debug.Log("Current Turn on client: " + currentTurn);
	} 
	
	[ClientRpc]
	public void RpcSyncPot(int potvalue) {
		m_potText.text = "$ " + potvalue.ToString();
	}
	
	[ClientRpc]
	public void RpcShowWinnerPanel(string m_winnerName, int[] winCards, string m_losersName, int[] losCards, int potValue) {
		m_winnerPanel.SetActive(true);
		var panelUI = m_winnerPanel.GetComponent<EndGamePanelUI>();
		panelUI.m_potText.text = "POT $ " + potValue.ToString();
		
		panelUI.m_winnerName.text = "WINNER HAND - " + m_winnerName;
		panelUI.m_losersName.text = "LOSER HAND - " + m_losersName;

		var wcards = m_allCards.FindAll(p=>(
			p.cardId == winCards[0] || 
			p.cardId == winCards[1] || 
			p.cardId == winCards[2] ));
		
		var lcards = m_allCards.FindAll(p=>(
			p.cardId == losCards[0] ||
			p.cardId == losCards[1] ||
			p.cardId == losCards[2] ));

		panelUI.m_wCard1.sprite = wcards[0].sprite;
		panelUI.m_wCard2.sprite = wcards[1].sprite;
		panelUI.m_wCard3.sprite = wcards[2].sprite;
		
		panelUI.m_lCard1.sprite = lcards[0].sprite;
		panelUI.m_lCard2.sprite = lcards[1].sprite;
		panelUI.m_lCard3.sprite = lcards[2].sprite;

	}
	
	[ClientRpc]
	public void RpcHideWinnerPanel() {
		m_winnerPanel.SetActive(false);
	}
	
	[Command]
	public void CmdIncreaseBet(int value) {
		m_lastBetTotal += value;
	}
	
	[Server]
	public void DecideWinner(Player p1, Player p2) {
		//p1.m_playerHandIds;//p1.GetHandCardIds();
		//p2.m_playerHandIds;//p2.GetHandCardIds();
		
		//Debug.Log("P1 CARDS. " + p1.m_playerHandIds.Count);
		//Debug.Log(string.Format("P2 CARDS. {0} {1} {2}", p2.m_playerHandIds[0], p2.m_playerHandIds[1], p2.m_playerHandIds[2]));
		
		var p1Cards = m_allCards.FindAll(p=>(
			p.cardId == p1.m_playerHandIds[0] || 
			p.cardId == p1.m_playerHandIds[1] || 
			p.cardId == p1.m_playerHandIds[2] ));
		
		var p2Cards = m_allCards.FindAll(p=>(
			p.cardId == p2.m_playerHandIds[0] ||
			p.cardId == p2.m_playerHandIds[1] ||
			p.cardId == p2.m_playerHandIds[2] ));
		
		var p1Hand = HandType(p1Cards);
		var p2Hand = HandType(p2Cards);
		
		Debug.Log(string.Format("PLAYER 1 HAND: {0}{1} {2}{3} {4}{5} : {6}", 
			p1Cards[0].value, p1Cards[0].suit.ToString()[0], 
			p1Cards[1].value, p1Cards[1].suit.ToString()[0],
			p1Cards[2].value, p1Cards[2].suit.ToString()[0],
			p1Hand));
		Debug.Log(string.Format("PLAYER 2 HAND: {0}{1} {2}{3} {4}{5} : {6}", 
			p2Cards[0].value, p2Cards[0].suit.ToString()[0], 
			p2Cards[1].value, p2Cards[1].suit.ToString()[0],
			p2Cards[2].value, p2Cards[2].suit.ToString()[0],
			p2Hand));
		Debug.Log("-------------------------");
		if (p1Hand == p2Hand) {
			if (BiggerHandOfSameType(p1Hand, p1Cards, p2Hand, p2Cards)) {
				p1.m_cash += m_pot;
				RpcShowWinnerPanel(p1.m_name, p1.m_playerHandIds.ToArray(), p2.m_name, p2.m_playerHandIds.ToArray(), m_pot);
			} else {
				p2.m_cash += m_pot;	
				RpcShowWinnerPanel(p2.m_name, p2.m_playerHandIds.ToArray(), p1.m_name, p1.m_playerHandIds.ToArray(), m_pot);
			}
		} else if ((int)p1Hand > (int)p2Hand) {
			p1.m_cash += m_pot;
			RpcShowWinnerPanel(p1.m_name, p1.m_playerHandIds.ToArray(), p2.m_name, p2.m_playerHandIds.ToArray(), m_pot);
		} else {
			p2.m_cash += m_pot;
			RpcShowWinnerPanel(p2.m_name, p2.m_playerHandIds.ToArray(), p1.m_name, p1.m_playerHandIds.ToArray(), m_pot);
		}
		Invoke("StartNewRound", 5f);
	}
	
	[Server]
	public bool BiggerHandOfSameType(HANDTYPE hand1, List<CardData> hand1Cards , HANDTYPE hand2, List<CardData> hand2Cards) {
		var p1Cards = hand1Cards.Select((v,i) => v.value).OrderBy(v=>v).ToList();
		var p2Cards = hand2Cards.Select((v,i) => v.value).OrderBy(v=>v).ToList();
		if (hand1 == hand2) {
			if (hand1 == HANDTYPE.HIGH_CARD) {
				// Check for highest card
				for (int i=2; i>=0; i--) 
					if (p1Cards[i] > p2Cards[i])
						return true;
					else if (p1Cards[i] < p2Cards[i])
						return false;
				return true; // todo: tie
			}
			if (hand1 == HANDTYPE.PAIR) {
				if (p1Cards[1] > p2Cards[1])
					return true;
				else if (p1Cards[1] < p2Cards[1])
					return false;
				else if (p1Cards.Sum() > p2Cards.Sum())
					return true;
				else if (p1Cards.Sum() < p2Cards.Sum())
					return false;
				return true;	// todo: tie
			}
			if (hand1 == HANDTYPE.FLUSH) {
				for (int i=2; i>=0; i--) 
					if (p1Cards[i] > p2Cards[i])
						return true;
					else if (p1Cards[i] < p2Cards[i])
						return false;
				return hand1Cards[0].suit > hand2Cards[0].suit;
			}
			if (hand1 == HANDTYPE.STRAIGHT) {
				for (int i=2; i>=0; i--) 
					if (p1Cards[i] > p2Cards[i])
						return true;
					else if (p1Cards[i] < p2Cards[i])
						return false;
				return hand1Cards[0].suit > hand2Cards[0].suit;
				// TODO: USE AS as 1 or 14
			}
			if (hand1 == HANDTYPE.STRAIGHT_FLUSH) {
				for (int i=2; i>=0; i--) 
					if (p1Cards[i] > p2Cards[i])
						return true;
					else if (p1Cards[i] < p2Cards[i])
						return false;
				return hand1Cards[0].suit > hand2Cards[0].suit;
			}
			if (hand1 == HANDTYPE.TRAIL) {
				return p1Cards[0] > p2Cards[0];
			}
			return true;
		} else {
			Debug.LogError("Trying to compare different hands.");
		}
		return false;
	}
	
	
	
	[Server]
	public HANDTYPE HandType(List<CardData> cards) {
		List<int> cardValues = new List<int>(new int[15]);
		List<int> cardSuits = new List<int>(new int[4]);
		
		cards.ForEach(p => {
			cardValues[p.value]+=1;
			cardSuits[(int)p.suit]+=1;
		});
		
		int valueCount = cardValues.Max();
		int suitsCount = cardSuits.Max();
		
		// Check if cards are in sequence
		bool isStraight = false;
		if (cardValues.FindAll(p=>p==1).Count() == 3) {
			int index = cardValues.IndexOf(1);
			isStraight = (cardValues[index+1] == 1 && cardValues[index+2] == 1);
		}
		
		// Check for 3 equal suits
		bool isFlush = (suitsCount == 3);
		
		// look from the strongest to the weakest combination
		if (valueCount == 3) {
			return HANDTYPE.TRAIL;
		}
		
		if (isStraight && isFlush) {
			return HANDTYPE.STRAIGHT_FLUSH;
		} else if (isStraight) {
			return HANDTYPE.STRAIGHT;
		} else if (isFlush) {
			return HANDTYPE.FLUSH;
		}
		
		if (valueCount == 1) {
			return HANDTYPE.HIGH_CARD;
		} else {
			return HANDTYPE.PAIR;
		}
	}
	
	[Server]
	public HANDTYPE GetWinnerHand(HANDTYPE hand1, HANDTYPE hand2) {
		if ((int)hand1 < (int)hand2) {
			return hand2;
		} else {
			return hand1;
		}
	}
	
	
	
	////// Client UI hooks ////////
	
	public void EndTurn() {
		m_localPlayer.CmdPlaceBetAndEndTurn();
	}
	
	public void IncreaseBet(int value) {
		m_localPlayer.CmdMakeBet(value);
	}
	
	public void Fold() {
		m_localPlayer.CmdFold();
	}
	
	public void Show() {
		m_localPlayer.CmdMakeBetToFill();
		m_localPlayer.CmdShowCards();
	}
	
	public void AllIn() {
		m_localPlayer.CmdAllIn();
		m_localPlayer.CmdShowCards();
	}
}
