﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;

public class JoinGame : MonoBehaviour {
	
	public Prototype.NetworkLobby.LobbyManager lobbyManager;
	public PasswordManager m_passwordManager;
	
	// Use this for initialization
	void Start () {
		Debug.Log(lobbyManager.matchMaker);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void OnGUIMatchList(bool success, string extendedInfo, List<MatchInfoSnapshot> matches)
	{
		if (matches.Count == 0) {
			Debug.LogError("No match was found for password: " + PasswordManager.s_passwordStr);
			//if (currentPage == 0) {
			//	noServerFound.SetActive(true);
			//}
			//currentPage = previousPage;
			return;
		}
		
		//noServerFound.SetActive(false);
		//foreach (Transform t in serverListRect)
		//	Destroy(t.gameObject);
		
		for (int i = 0; i < matches.Count; ++i)
		{
			Debug.Log("Found match with password " + PasswordManager.s_passwordStr);
			lobbyManager.matchMaker.JoinMatch(matches[i].networkId, "", "", "", 0, 0, lobbyManager.OnMatchJoined);
			lobbyManager.backDelegate = lobbyManager.StopClientClbk;
			//lobbyManager._isMatchmaking = true;
			lobbyManager.DisplayIsConnecting();
			
	//void JoinMatch(NetworkID networkID, LobbyManager lobbyManager)
	//{
	//	lobbyManager.matchMaker.JoinMatch(networkID, "", "", "", 0, 0, lobbyManager.OnMatchJoined);
	//	lobbyManager.backDelegate = lobbyManager.StopClientClbk;
	//	lobbyManager._isMatchmaking = true;
	//	lobbyManager.DisplayIsConnecting();
	//}
			//GameObject o = Instantiate(serverEntryPrefab) as GameObject;
			//o.GetComponent<LobbyServerEntry>().Populate(matches[i], lobbyManager, (i % 2 == 0) ? OddServerColor : EvenServerColor);
			//o.transform.SetParent(serverListRect, false);
		}
	}
	
	
	public void OnClickJoinGame() {
		if (PasswordManager.s_hasPassword) {
			Debug.Log("Looking for match with password: " + PasswordManager.s_passwordStr);
			lobbyManager.matchMaker.ListMatches(0, 1, "", false, 0, 0, OnGUIMatchList);
		} else {
			Debug.LogError("Not a valid password");
		}
	}
	
	public void OnClickPastePassword() {
		TextEditor te = new TextEditor();
		te.Paste();
		Debug.Log("pasted text: " + te.text);
		PasswordManager.SetPassword(te.text);
		m_passwordManager.DisplayCurrentPassword();
	}
}
