﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
//using UnityEngine.UI;


public class PasswordManager : MonoBehaviour {
	
	public static int[] s_password;
	public static string s_passwordStr { get {return PassArray2Str(s_password);}}
	public static bool s_hasPassword { get { return s_password != null; }}
	
	public bool m_allowToTypePassword = true;
	public List<TMP_InputField> m_passwordChar;
	
	private TouchScreenKeyboard m_keyboard;
	
	/*
	private TMP_InputField m_selectedInput {
		get {
			foreach(var input in m_passwordChar) {
				if (input.isFocused)
					return input;
			}
			return null;
		}
	}
	*/
	
	
	void Start() {
		foreach(var t in m_passwordChar) {
			t.interactable = m_allowToTypePassword;
		}
		if (s_hasPassword) {
			DisplayCurrentPassword();
		}
	}
	
	// Display the current password on the input fields
	public void DisplayCurrentPassword() {
		if (s_hasPassword) {
			if (s_password.Length == m_passwordChar.Count) {
				for (int i=0; i<s_password.Length; i++) {
					m_passwordChar[i].text = s_password[i].ToString();
				}
			} else {
				Debug.LogError("The password Length and Display Size don't match.");
			}
		} else {
			Debug.LogError("No password has been generated to display.");
		}
	}
	
	// Update the password based on the values that are on the input fields
	private void UpdatePasswordFromInputFields() {
		string str = "";
		for (int i=0; i<m_passwordChar.Count; i++) {
			str += m_passwordChar[i].text;
		}
		Debug.Log(str);
		ParsePasswordFromString(str);
	}
	
	
	// ================> STATIC METHODS ========================================>
	
	// Generate a password of a given size
	public static void GeneratePassword(int size) {
		s_password = new int[size];
		for (int i=0; i<size; i++) {
			s_password[i] = Random.Range(0,9);
		}
	}
	
	// Set password from a string value
	public static void SetPassword(string str) {
		ParsePasswordFromString(str);
	}
	
	// Given a string, set is as a password 
	private static void ParsePasswordFromString(string pass) {
		s_password = new int[pass.Length];
		for(int i=0; i<s_password.Length; i++) {
			try {
				int c = int.Parse(pass[i].ToString());
				s_password[i] = c;
			} catch (System.FormatException e) {
				s_password = null;
				Debug.LogWarning(e.Message);
				break;
			}
		}
	}
	
	// Convert the password from an int array to string
	private static string PassArray2Str(int[] pass) {
		string strPass = "";
		foreach (var c in pass) {
			strPass += c;
		}
		return strPass;
	}
	
	
	// =============== INPUT FIELD CALLBACK ====================================>
	public void OnValueChangeGoToNextInput(string value) {
		UpdatePasswordFromInputFields();
		/*
		if (value.Length == m_selectedInput.characterLimit) {
			Debug.Log("password");
			
		}	
		*/
	}
}
