﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This was done to fix a Unity bug where the keyboard focus is lost when changing the input field.
/// </summary>
public class InputFieldToPasswordFill : MonoBehaviour {
	
	public TMPro.TMP_InputField[] m_inputFields;
	
	public void OnTypeCharacter(string value) {
		for (int i=0; i<m_inputFields.Length; i++) {
			if (i < value.Length)
				m_inputFields[i].text = value[i].ToString();
			else 
				m_inputFields[i].text = "";
		}
	}
}
