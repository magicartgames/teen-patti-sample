﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "Card", menuName = "Card/New", order = 1)]
public class CardData : ScriptableObject, IComparable<CardData> {
	public enum CardSuit {Clubs, Diamonds, Hearts, Spades}
	
	public int cardId;
	public int value;
	public CardSuit suit;
	public Sprite sprite;
	
	public int CompareTo(CardData other) {
		return CardData.Compare(this, other); //(other == null)? 1 : other.value.CompareTo(value);
	}
	
	public int GetSuitId() {
		switch (suit) {
		case CardSuit.Clubs:
			return 1;
		case CardSuit.Spades:
			return 2;
		case CardSuit.Hearts:
			return 3;
		case CardSuit.Diamonds:
			return 4;
		}
		return 0;
	}
	
	public static int Compare(CardData left, CardData right)
	{
		if ((object)left == null)
			if ((object)right == null) 
				return 0;
			else 
				return -1;
		else if ((object)right == null) 
			return 1; 
		else
			return left.value.CompareTo(right.value); 
	}
	
	public static bool operator == (CardData a, CardData b){
		return CardData.Compare(a, b) == 0;
	}
	
	public static bool operator != (CardData a, CardData b){
		return !(CardData.Compare(a, b) == 0);
	}
	
	public static bool operator > (CardData a, CardData b){
		return CardData.Compare(a, b) > 0;
	}
	
	public static bool operator < (CardData a, CardData b){
		return CardData.Compare(a, b) < 0;
	}
	
	public static bool operator >= (CardData a, CardData b){
		return CardData.Compare(a, b) >= 0;
	}
	
	public static bool operator <= (CardData a, CardData b){
		return CardData.Compare(a, b) <= 0;
	}
}
