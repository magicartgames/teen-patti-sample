﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RemotePlayerUI : MonoBehaviour {
	
	public TextMeshProUGUI m_remotePlayersName;
	public TextMeshProUGUI m_remotePlayersCash;
	public TextMeshProUGUI m_remotePlayersLastBet;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
