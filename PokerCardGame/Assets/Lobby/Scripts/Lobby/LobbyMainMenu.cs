﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Prototype.NetworkLobby
{
	
    //Main menu, mainly only a bunch of callback called by the UI (setup throught the Inspector)
    public class LobbyMainMenu : MonoBehaviour 
    {
        public LobbyManager lobbyManager;

        public RectTransform lobbyServerList;
	    public RectTransform lobbyPanel;
	    public RectTransform m_joinGamePanel;

        public InputField ipInput;
	    public InputField matchNameInput;
	    
	    public PasswordManager m_passwordMgr;

        public void OnEnable()
        {
            lobbyManager.topPanel.ToggleVisibility(true);

            ipInput.onEndEdit.RemoveAllListeners();
            ipInput.onEndEdit.AddListener(onEndEditIP);

            matchNameInput.onEndEdit.RemoveAllListeners();
            matchNameInput.onEndEdit.AddListener(onEndEditGameName);
        }

        public void OnClickHost()
	    {
            lobbyManager.StartHost();
        }


        public void OnClickJoin()
        {
            lobbyManager.ChangeTo(lobbyPanel);

            lobbyManager.networkAddress = ipInput.text;
            lobbyManager.StartClient();

            lobbyManager.backDelegate = lobbyManager.StopClientClbk;
            lobbyManager.DisplayIsConnecting();

            lobbyManager.SetServerInfo("Connecting...", lobbyManager.networkAddress);
        }



        public void OnClickDedicated()
        {
            lobbyManager.ChangeTo(null);
            lobbyManager.StartServer();

            lobbyManager.backDelegate = lobbyManager.StopServerClbk;

            lobbyManager.SetServerInfo("Dedicated Server", lobbyManager.networkAddress);
        }
        

		// ==> THIS IS IMPORTANT
        public void OnClickCreateMatchmakingGame()
	    {
	    	// Manage Password
	    	PasswordManager.GeneratePassword(6);
		    m_passwordMgr.DisplayCurrentPassword();
		    
		    // Create Match
            lobbyManager.StartMatchMaker();
		    lobbyManager.matchMaker.CreateMatch(
	            // matchNameInput.text,
			    PasswordManager.s_passwordStr,
                (uint)lobbyManager.maxPlayers,
			    true,
				"", "", "", 0, 0,
				lobbyManager.OnMatchCreate);

            lobbyManager.backDelegate = lobbyManager.StopHost;
            lobbyManager._isMatchmaking = true;
            lobbyManager.DisplayIsConnecting();

            lobbyManager.SetServerInfo("Matchmaker Host", lobbyManager.matchHost);
	    }
	    
	    public void OnClickJoinGame() {
	    	lobbyManager.StartMatchMaker();
		    lobbyManager.backDelegate = lobbyManager.StopHost;
		    lobbyManager._isMatchmaking = true;
	    	lobbyManager.ChangeTo(m_joinGamePanel);
	    }

        public void OnClickOpenServerList()
        {
            lobbyManager.StartMatchMaker();
            lobbyManager.backDelegate = lobbyManager.SimpleBackClbk;
            lobbyManager.ChangeTo(lobbyServerList);
        }

        void onEndEditIP(string text)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                OnClickJoin();
            }
        }

        void onEndEditGameName(string text)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                OnClickCreateMatchmakingGame();
            }
        }

    }
}
