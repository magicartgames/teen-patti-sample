﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class EndGamePanelUI : MonoBehaviour {
	
	public TextMeshProUGUI m_potText;
	[Header("text")]
	public TextMeshProUGUI m_winnerName;
	public TextMeshProUGUI m_losersName;
	
	[Header("Winners Cards")]
	public Image m_wCard1;
	public Image m_wCard2;
	public Image m_wCard3;
	
	[Header("Losers Cards")]
	public Image m_lCard1;
	public Image m_lCard2;
	public Image m_lCard3;
	
}
