﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prototype.NetworkLobby;
using UnityEngine.Networking;

public class NetworkLobbyHookPlayer : LobbyHook {
	
	public override void OnLobbyServerSceneLoadedForPlayer(NetworkManager manager, GameObject lobbyPlayer, GameObject gamePlayer)
	{
		//base.OnLobbyServerSceneLoadedForPlayer(manager, lobbyPlayer, gamePlayer);
		LobbyPlayer lobby = lobbyPlayer.GetComponent<LobbyPlayer>();
		Player player = gamePlayer.GetComponent<Player>();
		
		player.m_name = lobby.playerName;
		player.m_color = lobby.playerColor;
		//spaceship.name = lobby.name;
		//spaceship.color = lobby.playerColor;
		//spaceship.score = 0;
		//spaceship.lifeCount = 3;
	}
}
