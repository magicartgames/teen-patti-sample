﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class LocalPlayerUI : MonoBehaviour {
	
	public TextMeshProUGUI m_playerCash;
	public TextMeshProUGUI m_playerName;
	public TextMeshProUGUI m_lastBet;
	
	public Button m_betBtn;
	public Button m_foldBtn;
	public Button m_showCards;
	public Button m_allIn;
	public Button m_bet50;
	public Button m_bet100;
	public List<CardSlot> m_cardSlots;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
