﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Networking;

public class Player : NetworkBehaviour {
	
	[SyncVar(hook="OnAddCash")]
	public int m_cash;
	[SyncVar(hook="OnMakeBet")]
	public int m_currentBet;
	//[SyncVar(hook="OnMinBet")]
	//public int m_minBet;
	
	[SyncVar]
	public bool m_anteBet = true;
	
	//public TextMeshProUGUI m_cashText;
	private LocalPlayerUI m_localUI;
	private RemotePlayerUI m_remoteUI;
	
	public bool m_isBlind = true;
	public List<CardSlot> m_playerHand;
	[SyncVar] public SyncListInt m_playerHandIds;
	
	// Variables synced from lobby
	[SyncVar]
	public string m_name;
	[SyncVar]
	public Color m_color;
	
	
	void Start() {
		StartCoroutine(InitializePlayerData());
	}
	
	public IEnumerator InitializePlayerData() {
		// Wait for data to get ready (Unity lixo)
		yield return new WaitForSeconds(0.25f);
		if (isServer) {
			//Debug.Log("Starting Server.");
			MatchManager._instance.AddPlayer(this);
		}
		if (isLocalPlayer) {
			gameObject.name = "LocalPlayer";
			m_localUI = FindObjectOfType<LocalPlayerUI>();
			//Debug.Log("CALLING GET CARD COMMANDS");
			m_playerHandIds.Callback += SetCardForPlayer;
			
			m_cash = 1000;
			m_currentBet = 0;
			m_localUI.m_playerCash.text = m_cash.ToString();
			m_localUI.m_playerName.text = m_name;
			//m_localUI.m_betBtn.onClick.AddListener(delegate{MatchManager._instance.EndTurn(m_name);});
			//m_localUI.m_bet50.onClick.AddListener(delegate{CmdAddCash(-50);});
			//m_localUI.m_bet100.onClick.AddListener(delegate{CmdAddCash(-100);});
			
			m_playerHand = new List<CardSlot>(m_localUI.GetComponentsInChildren<CardSlot>());
			m_localUI.m_cardSlots.ForEach(card => card.e_OnClickCard += ShowCards);
			
			MatchManager._instance.AddLocalPlayer(this);
			
			//Debug.Log("Starting Local player.");
			//Debug.Log("Setting up player: " + m_name);
			//Debug.Log("Player color: " + m_color);
		} else {
			m_remoteUI = FindObjectOfType<RemotePlayerUI>();
			m_remoteUI.m_remotePlayersName.text = m_name;
			m_remoteUI.m_remotePlayersCash.text = m_cash.ToString();
			MatchManager._instance.AddRemotePlayer(this);
		}
	}
	
	
	void Awake () {
		
	}
	
	void Update() {
		
		//if (!isLocalPlayer) {
		//	Debug.Log("remote player name: " + m_name);
		//	m_remoteUI.m_remotePlayersName.text = m_name;
		////	if (Input.GetKeyDown(KeyCode.U)) {
		////		CmdAddCash(-50);
		////		//FindObjectOfType<MatchManager>().EndTurn();
		////	}
		//}
	}
	
	public void SetCardForPlayer(UnityEngine.Networking.SyncList<int>.Operation op, int index) {
		if (op == SyncList<int>.Operation.OP_ADD) {
			//Debug.Log("SYNCING CARD: " + m_name + " - " + index + " " + (int)m_playerHandIds[index]);
			m_playerHand[index].SetCartData(MatchManager._instance.GetCard((int)m_playerHandIds[index]));
		} else if (op == SyncList<int>.Operation.OP_CLEAR) {
			//Debug.Log("CLEARING LIST === NOOOOOOOOO ===" + m_name);
			m_playerHand.ForEach(p=>p.SetCartData(null));
		}
	}
	
	[Command]
	public void CmdGetCardsFromServer() {
		//Debug.Log("DISTRIBUTING CARDS FOR PLAYER: " + m_name);
		m_playerHandIds.Clear();
		for (int i=0; i<3; i++) {
			m_playerHandIds.Add(MatchManager._instance.PickCardFromDeck().cardId);
		}
		
	}
	
	public void OnAddCash(int currentCash) {
		m_cash = currentCash;
		if (isLocalPlayer) {
			m_localUI.m_playerCash.text = "$" + m_cash.ToString();
		}
		else {
			m_remoteUI.m_remotePlayersCash.text = "$" + m_cash.ToString();
		}
	}
	
	public void OnMakeBet(int currentBet) {
		m_currentBet = currentBet;
		//bool zeroingBet = currentBet == 0;
		//bool endbetting = currentBet < m_lastBet;
		
		
		//Debug.Log("Making bet: last bet" + m_lastBet + " -- min bet" + m_minBet);
		//if (isLocalPlayer){
		//	if (zeroingBet)
		//		m_localUI.m_lastBet.text = m_lastBet.ToString();
		//	else if (endbetting)
		//		m_localUI.m_lastBet.text = (zeroingBet? "+$": "Bet: $") + m_lastBet.ToString();
		//	else
		//		m_localUI.m_lastBet.text = "Bet: " + m_lastBet.ToString();
			 
		//	if (!m_anteBet && !endbetting) {
		//		m_localUI.m_showCards.interactable = m_lastBet <= m_minBet;	
		//		m_localUI.m_betBtn.interactable = (m_lastBet > m_minBet);
		//	}
		//}
		//else {
		//	if (m_lastBet == 0)
		//		m_remoteUI.m_remotePlayersLastBet.text = "-";
		//	else
		//		m_remoteUI.m_remotePlayersLastBet.text = endbetting? "Last: +$" + m_lastBet.ToString() : "0";
		//}
	}
	
	
	//public void OnMinBet(int currentMinBet) {
	//	m_minBet = currentMinBet;
	//}
	
	/// <summary>
	/// Clicked on Bet chips
	/// </summary>
	//public void Bet(int value) {
	//	CmdAddCash(-value);
	//	//PotManager.PlaceBet(this, value);
	//}
	
	[Server]
	public int[] GetHandCardIds() {
		int[] hand = new int[3];
		for (int i=0; i < m_playerHand.Count; i++) {
			if (m_playerHand[i].m_card != null) {
				hand[i] = m_playerHand[i].m_card.cardId;
			}
		}
		return hand;
	}
	
	[Command]
	public void CmdMakeBet(int value) {
		int minBet = MatchManager._instance.m_lastBetTotal;
		Player p = MatchManager._instance.m_currentTurnPlayer;
		//Debug.Log("LAST BET: " + minBet);
		
		// Increase current bet by value
		m_currentBet += value;
		
		m_anteBet = false;
		//Debug.Log("Other player bet: " + otherPlayerLastBet);
		
		//m_minBet = minBet;
		m_cash -= value;
		RpcUpdateButtons(minBet, m_currentBet, m_cash, false, (p!=null)? p.m_name : "NULL");
		//if (isLocalPlayer) {
		//	if (m_lastBet == m_minBet)
		//		m_localUI.m_showCards.interactable = true;
		//	else if (m_lastBet > m_minBet)
		//		m_localUI.m_showCards.interactable = true;
		//}
		MatchManager._instance.IncreasePot(value);
	}
	
	[Command]
	public void CmdMakeBetToFill() {
		int minBet = MatchManager._instance.m_lastBetTotal;
		Player p = MatchManager._instance.m_currentTurnPlayer;
		int value = minBet - m_currentBet;
		//Debug.Log("FILL TO COMPLETE THE MIN BET: " + value);
		m_currentBet += value;
		m_anteBet = false;
		m_cash -= value;
		RpcUpdateButtons(minBet, m_currentBet, m_cash, false, (p!=null)? p.m_name : "NULL");
		//if (isLocalPlayer) {
		//	if (m_lastBet == m_minBet)
		//		m_localUI.m_showCards.interactable = true;
		//	else if (m_lastBet > m_minBet)
		//		m_localUI.m_showCards.interactable = true;
		//}
		MatchManager._instance.IncreasePot(value);
	}
	
	[Command]
	public void CmdAllIn() {
		int minBet = m_cash; //MatchManager._instance.m_lastBetTotal;
		Player p = MatchManager._instance.m_currentTurnPlayer;
		//int value = minBet - m_currentBet;
		//Debug.Log("FILL TO COMPLETE THE MIN BET: " + value);
		m_currentBet += m_cash;
		m_anteBet = false;
		m_cash = 0;
		RpcUpdateButtons(minBet, m_currentBet, m_cash, false, (p!=null)? p.m_name : "NULL");
		//if (isLocalPlayer) {
		//	if (m_lastBet == m_minBet)
		//		m_localUI.m_showCards.interactable = true;
		//	else if (m_lastBet > m_minBet)
		//		m_localUI.m_showCards.interactable = true;
		//}
		MatchManager._instance.IncreasePot(m_currentBet);
	}
	
	[Command]
	public void CmdShowCards() {
		MatchManager._instance.ShowCards();
		
		//RpcDisplayCards();
	}
	
	[ClientRpc]
	public void RpcDisplayCards() {
		Debug.Log(string.Format(m_name + " CARDS. {0} {1} {2}", m_playerHandIds[0], m_playerHandIds[1], m_playerHandIds[2]));
	}
	
	[ClientRpc]
	public void RpcUpdateButtons(int minBet, int betValue, int playerCash, bool antebet, string pname) {
		//bool zeroingBet = currentBet == 0;
		//bool endbetting = currentBet < m_lastBet;
		bool isMyTurn = pname == m_name;
		//Debug.Log("PLAYER CURRENT TURN NAME: " + pname);
		if (isLocalPlayer){
			if (isMyTurn) {
				m_localUI.m_lastBet.text ="Bet: +$" + betValue.ToString();
			}
			//if (zeroingBet)
			//	m_localUI.m_lastBet.text = m_lastBet.ToString();
			//else if (endbetting)
			//	m_localUI.m_lastBet.text = (zeroingBet? "+$": "Bet: $") + m_lastBet.ToString();
			//else
			//	m_localUI.m_lastBet.text = "Bet: " + m_lastBet.ToString();
			//Debug.Log("LAST BET: " + betValue + " --- MIN BET: " + minBet);
			if (!antebet) {
				m_localUI.m_showCards.interactable = betValue <= minBet;	
				m_localUI.m_betBtn.interactable = (betValue > minBet);
			}
			if (playerCash <= 0) {
				m_localUI.m_bet50.interactable = false;
				m_localUI.m_bet100.interactable = false;
			} else if (playerCash <= 50) {
				m_localUI.m_bet100.interactable = false;
			} else {
				m_localUI.m_bet50.interactable = true;
				m_localUI.m_bet100.interactable = true;
			}
		}
		//else {
		//	if (!isMyTurn){
		//		m_remoteUI.m_remotePlayersLastBet.text = "Last: +$" + minBet.ToString();
		//	} else {
		//		m_remoteUI.m_remotePlayersLastBet.text = "-";
		//	}
		//	//if (betValue == 0)
		//	//	m_remoteUI.m_remotePlayersLastBet.text = "-";
		//	//else
		//	//	m_remoteUI.m_remotePlayersLastBet.text ="Last: +$";
		//}
	}
	
	[Command]
	public void CmdAnteBet(int value) {
		m_anteBet = true;
		//Debug.Log("Initial bet");
		m_cash -= value;
		m_currentBet = value;
		//minBet = 50;
		MatchManager._instance.IncreasePot(value);
		//Player p = MatchManager._instance.m_currentTurnPlayer;
		RpcUpdateButtons(50, m_currentBet, m_cash, true, "");
	}
	
	[ClientRpc]
	public void RpcInitializeRound() {
		if (isLocalPlayer) {
			//Debug.Log("GETTING CARDSSSSSSSSSS FOR PLAYERRRRRRR: " + m_name);
			CmdGetCardsFromServer();
			m_localUI.m_foldBtn.interactable = false;
			m_localUI.m_betBtn.interactable = false;
			m_localUI.m_bet50.interactable = true;
			m_localUI.m_bet100.interactable = true;
			m_localUI.m_showCards.gameObject.SetActive(true);
			m_localUI.m_showCards.interactable = false;
			m_localUI.m_allIn.gameObject.SetActive(false);
		}
	}

	[Command]
	public void CmdPlaceBetAndEndTurn() {
		//print("This should print on the server only....");
		//RpcGoToNextTurn();
		//m_currentBet -= m_minBet;
		MatchManager._instance.ServerNextPlayerTurn();
	}
	
	[Command]
	public void CmdClearLastBet() {
		m_currentBet = 0;
	}
	
	[Command]
	public void CmdFold() {
		MatchManager._instance.PlayerFold(m_name);
	}
	
	//[Command]
	//public void CmdSetMinBet(int minBet) {
	//	m_minBet = minBet;
	//	Debug.Log(minBet);
	//}
	//[ClientRpc]
	//public void RpcGoToNextTurn() {
	//	//if (this == MatchManager._instance.m_)
	//	MatchManager._instance.ServerNextPlayerTurn();
	//	Debug.Log("This is bullshit: " + m_name);
	//	//MatchManager._instance.ServerNextPlayerTurn();
	//}
	
	public void ShowCards(CardSlot cardSlot) {
		m_playerHand.ForEach(p=>p.SetHidden(!p.m_isHidden));
	}
	
	[ClientRpc]
	public void RpcPlayerTurn(string playerTurnsName, int minBet) {
		bool isMyTurn = playerTurnsName == m_name;
		//Debug.Log("Calling RPC on players." + m_name + " is my turn: " + isMyTurn.ToString());
		if (isLocalPlayer) {
			m_localUI.m_foldBtn.interactable = isMyTurn;
			m_localUI.m_bet50.interactable = isMyTurn;
			m_localUI.m_bet100.interactable = isMyTurn;
			m_localUI.m_showCards.interactable = isMyTurn;
			//m_localUI.m_betBtn.interactable = false;
			//m_localUI.m_showCards.interactable = false;
			//m_localUI.m_showCards.interactable = isMyTurn;
			if (isMyTurn) {
				CmdClearLastBet();
				// Need to check if have money to go
				if (m_cash <= minBet) {
					m_localUI.m_showCards.gameObject.SetActive(false);
					m_localUI.m_allIn.gameObject.SetActive(true);
					m_localUI.m_allIn.interactable = true;
				}
			} else {
				m_localUI.m_betBtn.interactable = false;
				//m_localUI.m_showCards.interactable = false;
			}
		}
		UpdateTextChangeTurn(isMyTurn, minBet);
	}
	
	[Client]
	void UpdateTextChangeTurn(bool myTurn, int minBet) {
		if (isLocalPlayer) {
			if (myTurn)
				m_localUI.m_lastBet.text = "Bet: $0";
		} else {
			if (!myTurn)
				m_remoteUI.m_remotePlayersLastBet.text = "Last bet: +$" + minBet;
			else
				m_remoteUI.m_remotePlayersLastBet.text = "-";
		}
	}
	
	//[Client]
	//public void SetMinBet(int minBet) {
	//	CmdSetMinBet(minBet);
	//}
}
