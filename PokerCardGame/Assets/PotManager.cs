﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PotManager : MonoBehaviour {
	private static PotManager _instance;
	public TextMeshProUGUI m_PotText;
	public int m_potValue;
	
	void Awake () {
		_instance = this;
		m_potValue = 0;
		m_PotText.text = m_potValue.ToString();
	}
	
	public static void PlaceBet(Player player, int value) {
		AddValueToPot(value);
	}
	
	private static void AddValueToPot(int value) {
		_instance.m_potValue += value;
		_instance.m_PotText.text = _instance.m_potValue.ToString();
	}
}
