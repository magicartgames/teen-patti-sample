﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PlayerHand : MonoBehaviour {

	public bool m_isBlind = true;
	public List<CardSlot> m_playerHand;
	
	
	// Use this for initialization
	void Start () {
		m_playerHand.ForEach(card => card.e_OnClickCard += ShowCards);
	}
	
	/// <summary>
	/// If player clicks on any card, the cards are shown to him (exits blind mode)
	/// </summary>
	public void ShowCards(CardSlot cardSlot) {
		if (m_isBlind) {
			m_isBlind = false;
			m_playerHand.ForEach(card => card.SetCartData(MatchManager._instance.PickCardFromDeck()));
		}
	}
}
